/*!
// Testweb v0.3.0
// http://or-change.cn
// Copyright 2014 OrChange Inc. All rights reserved.
// Licensed under the GPL License.
*/
/*jslint vars: true, forin: true, plusplus: true*/
/*global define: false */
define(function (require, exports, module) {
	'use strict';
	require("lib/core/controller");
	require("lib/jquery/jsrender");
	require("lib/jquery/jquery.hashchange");
	require("lib/jquery/jquery.serializeObject");
	require("lib/jquery/jquery.cookie");
});
